import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const datasourceMSO: SchemaDefinition = {
    id: { type: Number, required: true },
    imgFileSize: { type: Number, required: true },
    lastUpdated: { type: Number, required: true },
    lat: { type: Number, required: true },
    lng: { type: Number, required: true },
    name: { type: String, required: true },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        address: {
            address_country: { type: String },
            address_formatted: { type: String },
            address_locality: { type: String },
            address_region: { type: String },
            postal_code: { type: String },
            street_address: { type: String },
        },
        district: { type: String },
        id: { type: Number, required: true },
        image: {
            data: { type: String },
            file_size: { type: Number, required: true },
            type: { type: String },
            url: { type: String, required: true },
        },
        last_updated: { type: Number, required: true },
        name: { type: String, required: true },
        updated_at: { type: Number, required: true },
    },
    type: { type: String, required: true },
};

const outputHistoryMSO: SchemaDefinition = {
    id: { type: Number, required: true },
    image: {
        data: { type: String },
        file_size: { type: Number, required: true },
        type: { type: String },
        url: { type: String, required: true },
    },
    last_updated: { type: Number, required: true },
    updated_at: { type: Number, required: true },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    history: {
        mongoCollectionName: "trafficcameras_history",
        name: "TrafficCamerasHistory",
        outputMongooseSchemaObject: outputHistoryMSO,
    },
    mongoCollectionName: "trafficcameras",
    name: "TrafficCameras",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as TrafficCameras };

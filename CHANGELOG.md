# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.6] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.5] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.4] - 2022-05-18

### Fixed

-   Axios request image/jpeg responseType ([traffic-cameras#2](https://gitlab.com/operator-ict/golemio/code/modules/traffic-cameras/-/issues/2))

## [1.0.3] - 2022-02-16

### Changed

-   Request-promise deprecated to Axios ([core#24](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/24))

import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { GeocodeApi } from "@golemio/core/dist/integration-engine/helpers";
import { TrafficCameras } from "#sch/index";
import { TrafficCamerasWorker } from "#ie/TrafficCamerasWorker";

describe("TrafficCamerasWorker", () => {
    let worker: TrafficCamerasWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: number[];
    let testTransformedHistoryData: number[];
    let data0: Record<string, any>;
    let data1: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: { now: new Date("2021-09-20T16:00:00.000Z") } });

        testData = [1, 2];
        testTransformedData = [1, 2];
        testTransformedHistoryData = [1, 2];
        data0 = { properties: { id: 0 }, geometry: { coordinates: [0, 0] } };
        data1 = { properties: { id: 1 }, geometry: { coordinates: [1, 1] }, save: sandbox.stub().resolves(true) };

        worker = new TrafficCamerasWorker();

        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["transformation"], "transformHistory").callsFake(() => Promise.resolve(testTransformedHistoryData));
        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker["historyModel"], "save");
        sandbox.stub(worker["historyModel"], "delete");
        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + TrafficCameras.name.toLowerCase();
        sandbox.stub(worker["model"], "findOneById").callsFake(() => Promise.resolve(data1));

        sandbox.stub(worker["cityDistrictsModel"], "findOne");
        sandbox.stub(GeocodeApi, "getAddressByLatLng");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.calledWith(worker["model"].save as SinonSpy, testTransformedHistoryData);
        sandbox.assert.calledThrice(worker["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.calledWith(
            worker["sendMessageToExchange"] as SinonSpy,
            "workers." + queuePrefix + ".saveDataToHistory",
            JSON.stringify(testTransformedData)
        );
        testTransformedData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateAddressAndDistrict",
                JSON.stringify(f)
            );
        });
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by saveDataToHistory method", async () => {
        await worker.saveDataToHistory({ content: Buffer.from(JSON.stringify(testTransformedData)) });
        sandbox.assert.calledOnce(worker["transformation"].transformHistory as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transformHistory as SinonSpy, testTransformedData);
        sandbox.assert.calledOnce(worker["historyModel"].save as SinonSpy);
        sandbox.assert.calledWith(worker["historyModel"].save as SinonSpy, testTransformedHistoryData);
        sandbox.assert.callOrder(worker["transformation"].transformHistory as SinonSpy, worker["historyModel"].save as SinonSpy);
    });

    it("should calls the correct methods by updateAddressAndDistrict method (different geo)", async () => {
        await worker.updateAddressAndDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.calledOnce(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(GeocodeApi.getAddressByLatLng as SinonSpy);
        sandbox.assert.calledTwice(data1.save);
    });

    it("should calls the correct methods by updateAddressAndDistrict method (same geo)", async () => {
        data1 = {
            geometry: { coordinates: [0, 0] },
            properties: {
                address: { address_formatted: "a" },
                district: "praha-0",
                id: 1,
            },
            save: sandbox.stub().resolves(true),
        };
        await worker.updateAddressAndDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.notCalled(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.notCalled(GeocodeApi.getAddressByLatLng as SinonSpy);
        sandbox.assert.notCalled(data1.save);
    });

    it("should call the correct methods by deleteOldTrafficCamerasHistory method", async () => {
        data1 = {
            geometry: { coordinates: [0, 0] },
            properties: {
                address: { address_formatted: "a" },
                district: "praha-0",
                id: 1,
            },
            save: sandbox.stub().resolves(true),
        };
        await worker.deleteOldTrafficCamerasHistory();
        sandbox.assert.calledOnce(worker["historyModel"].delete as SinonSpy);
        sandbox.assert.calledWith(worker["historyModel"].delete as SinonSpy, { updated_at: { $lt: 1632110400000 } });
    });
});
